<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username']) || !isset($_SESSION['type'])) {
			header("Location:" . base_url("welcome/login"));
        }
        $this->load->model("student_model", 'student');
        $this->load->model("question_model", 'question');
        $this->load->model("school_model", 'school');
        $this->load->model("statistic_model", 'statistic');   
    }

    public function index()
    {
        $student = $this->student->getProfile();
        $this->load->library('pagination');
        $page = 1;
		$perPage = 10;
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
        }
        $question = $this->question->getAll($page, $perPage);
        $results = $question['results'];
        $total = ceil($question['total'] / $perPage);
        $appName = $this->config->item("appName");
        $startNum = ($page - 1 ) * $perPage + 1;
        $data = array('appName' => $appName, 'question' => $results, 'num' => $startNum, 'type' => $this->session->userdata("type"));
        if ($student->studentHasTested == 1) {
            header("Location:" . base_url());
        }else{
            $data['tested'] = 0;
        }
        $profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
        if ($page == $total) {
            $data['endPage'] = true;
        }else{
            $data['endPage'] = false;
        }

        $config['base_url'] = base_url() . 'quiz';
        $config['total_rows'] = $question['total'];
        $config['per_page'] = $perPage;
        $config['num_links'] = 3;

        $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = '<span aria-hidden="true">First</span>';
        $config['last_link'] = '<span aria-hidden="true">Last</span>';
        $config['next_link'] = '<span aria-hidden="true">Next</span>';
        $config['prev_link'] = '<span aria-hidden="true">Prev</span>';

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['reuse_query_string'] = true;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $this->pagination->initialize($config);
        $data['links'] =  $this->pagination->create_links();

		$this->load->view('part/header', $data);
		$this->load->view('pages/student/quiz', $data);
		$this->load->view('part/footer', $data);
    }

    public function doAnswer()
    {
        $questionId = xss_clean($this->input->post("questionId"));
        $categoryId = xss_clean($this->input->post("categoryId"));
        $value = xss_clean($this->input->post("value"));

        $question = $this->question->getById($questionId);

        if ($question->favourable == 0) {
            $value *= -1;
        }

        if (isset($_SESSION['answers'])) {
            $answers = $this->session->userdata("answers");
            $exists = false;
            $index = null;
            for ($i=0; $i < count($answers); $i++) {
                if ($answers[$i]['questionId'] == $questionId) {
                    $exists = true;
                    $index = $i;
                    break;
                }
            }

            if ($exists == true) {
                $answer = array(
                    "questionId" => $questionId,
                    "categoryId" => $categoryId,
                    "value" => $value
                );
                $answers[$index] = $answer;
                $this->session->set_userdata('answers', $answers);
                $exists = false;
            }else{
                $answer = array(
                    "questionId" => $questionId,
                    "categoryId" => $categoryId,
                    "value" => $value
                );
                array_push($answers, $answer);
                $this->session->set_userdata('answers', $answers);
            }

        }else if (!isset($_SESSION['answers'])) { //When answer not exists
            $answers = [];
            $answer = array(
                "questionId" => $questionId,
                "categoryId" => $categoryId,
                "value" => $value
            );
            $answers[0] = $answer;
            $this->session->set_userdata('answers', $answers);
        }
    }

    public function total()
    {
        if(!isset($_SESSION['answers'])){
            header("Location:" . base_url());
        }else{
            $category = $this->question->getAllCategory();
            $answers = $_SESSION['answers'];

            for ($i=1; $i < count($answers) + 1; $i++) {    
                for ($a=0; $a < count($category); $a++) { 
                    if (!array_key_exists("student_username", $category[$a])) {
                        $category[$a]->student_username = $this->session->userdata('username');
                    }
                    if (!array_key_exists("value", $category[$a])) {
                        $category[$a]->value = 0;
                    }
                    unset($category[$a]->categoryName);
                    if (isset($answers[$i]['categoryId']) && $answers[$i]['categoryId'] == $category[$a]->category_id) {
                        if (array_key_exists("value", $category[$a])) {
                            $category[$a]->value += $answers[$i]['value'];
                        }else{
                            $category[$a]->value = 0;
                            $category[$a]->value += $answers[$i]['value'];
                        }
                        if (!array_key_exists("student_username", $category[$a])) {
                            $category[$a]->student_username = $this->session->userdata('username');
                        }
                        // Don't show logs answers
                        // if (array_key_exists("answers", $category[$a])) {
                        //     array_push($category[$a]->answers, $answers[$i]);
                        // } else {
                        //     $category[$a]->answers = array();
                        //     array_push($category[$a]->answers, $answers[$i]);
                        // }
                    }
                }
            }

            $statistic = array();

            for ($i=0; $i < count($category); $i++) { 
                if (array_key_exists("student_username", $category[$i])) {
                    array_push($statistic, $category[$i]);
                    if ($category[$i]->value < 0) {
                        $category[$i]->value = 0;
                    }
                }
            }
            if ($this->statistic->add($statistic) == true) {
                header("Location:" . base_url("?message=success"));
            } else {
                header("Location:" . base_url("quiz"));
            }
        }
    }
}
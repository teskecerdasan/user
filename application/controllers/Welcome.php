<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("teacher_model", 'teacher');
		$this->load->model("student_model", 'student');
		$this->load->model("statistic_model", 'statistic');
		$this->load->model("question_model", 'question');
		$this->load->model("school_model", 'school');
	}

	public function index()
	{
		if (!isset($_SESSION['username']) || !isset($_SESSION['type'])) {
			header("Location:" . base_url("welcome/login"));
		}

		$profile = $this->student->getProfile();

		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'type' => $this->session->userdata("type"));
		$data['tested'] = $profile->studentHasTested;
		$student = $this->student->getProfile();
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);

		if ($student->studentHasTested == 1) {
			$statistic = $this->statistic->getFromStudent();
			$category = [];
			$num = [];
			for ($i=0; $i < count($statistic); $i++) { 
				array_push($category, $statistic[$i]->categoryName);
				array_push($num, $statistic[$i]->statisticValue);
			}
			$data['label'] = $category;
			$data['num'] = $num;
			$this->load->view('pages/chart', $data);	
		}else{
			$category = $this->question->getAllCategory();
			$data['category'] = $category;
			$this->load->view('pages/index', $data);
		}
		
		$this->load->view('part/footer', $data);
	}

	public function category()
	{
		$id = $this->uri->segment(2);
		$appName = $this->config->item("appName");
		$category = $this->question->getCategoryById($id)[0];

		$data = array('appName' => $appName, 'category' => $category, 'type' => $this->session->userdata("type"));
		$profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);
		$this->load->view('pages/category', $data);
		$this->load->view('part/footer', $data);
	}

	public function profile()
	{
		$user;
		if ($_SESSION['type'] == "teacher") {
			$user = $this->teacher->get();
		} else if ($_SESSION['type'] == "student") {
			$user = $this->student->getProfile();
		}
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'type' => $this->session->userdata("type"));
		$profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
			$data['student'] = $user;
			$data['tested'] = $profile->studentHasTested;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
			$data['teacher'] = $user;
		}
		$this->load->view('part/header', $data);

		if ($_SESSION['type'] == "teacher") {
			$this->load->view('pages/profileTeacher', $data);
		} else if ($_SESSION['type'] == "student") {
			$this->load->view('pages/profileStudent', $data);
		}

		$this->load->view('part/footer', $data);
	}

	public function changePassword()
	{
		if (!$_POST) {
			header("Location:" . base_url());
		}else{
			$username = $_SESSION['username'];
			$type = $_SESSION['type'];
			$oldPassword = xss_clean($this->input->post("oldPass"));

			if ($type == "teacher") {	
				$userData = $this->teacher->getPassword($username);
				$passDb = $userData->password;
				
				if (password_verify($oldPassword, $passDb) == true) {	
					$newPassword = password_hash($this->input->post("newPass"), PASSWORD_BCRYPT);
					if ($this->teacher->changePassword($newPassword) == true) {
						echo "true";
					} else {
						echo "false";
					}
				}else{
					echo "Tidak";
				}
			}elseif ($type == "student") {
				$userData = $this->student->getPassword($username);
				$passDb = $userData->password;
				
				if (password_verify($oldPassword, $passDb) == true) {
					$newPassword = password_hash($this->input->post("newPass"), PASSWORD_BCRYPT);
					if ($this->student->changePassword($newPassword) == true) {
						echo "true";
					} else {
						echo "false";
					}
				}else{
					echo "Tidak";
				}
	
			}
		}
	}

	public function doEditProfileTeacher()
	{
		$data = array('name' => xss_clean($this->input->post("name")));
		if ($this->teacher->editProfile($data) == true) {
			header("Location:" . base_url("welcome/profile?message=success-edit-profile"));
		}else{
			header("Location:" . base_url("welcome/profile?message=error-edit-profile"));
		}
	}

	public function login()
	{
		$message = "";
		if (isset($_SESSION['error'])) {
			$message = $_SESSION['error'];
		}
		if (isset($_SESSION['username']) && isset($_SESSION['type'])) {
			header("Location:" . base_url("welcome"));
		}
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'message' => $message);
		$this->load->view('pages/login', $data);
	}

	public function register()
	{
		if (isset($_SESSION['username']) && isset($_SESSION['type'])) {
			header("Location:" . base_url("welcome"));
		}
		$schools = $this->school->getAll(1, 100);
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'schools' => $schools);
		$this->load->view('pages/register', $data);
	}

	public function doLogin()
	{
		$username = xss_clean($this->input->post("_username"));
		$password = xss_clean($this->input->post("_password"));

		if ($this->input->post("type") == "teacher") {	
			$userData = $this->teacher->getPassword($username);
			$passDb = $userData->password;
			
			if (password_verify($password, $passDb) == true) {
				$data = array(
						'username' => $username,
						'type' => $this->input->post("type")
				);
				$this->session->set_userdata($data);
				header("Location:" . base_url());
			}else{
				$this->session->set_flashdata('error', 'Wrong password');
				header("Location:" . base_url("welcome/login"));
			}
		}elseif ($this->input->post("type") == "student") {
			$userData = $this->student->getPassword($username);
			$passDb = $userData->password;
			
			if (password_verify($password, $passDb) == true) {
				$data = array(
						'username' => $username,
						'type' => $this->input->post("type")
				);
				$this->session->set_userdata($data);
				header("Location:" . base_url());
			}else{
				$this->session->set_flashdata('error', 'Wrong password');
				header("Location:" . base_url("welcome/login"));
			}

		}else{
			header("Location:" . base_url("welcome/login?error-message=error-type"));
		}
	}

	public function doRegister()
	{
		$username = xss_clean($this->input->post("_username"));
		$password = password_hash(xss_clean($this->input->post("_password")), PASSWORD_BCRYPT);
		$name = xss_clean($this->input->post("_name"));
		$nisn = xss_clean($this->input->post("_nisn"));
		$school = xss_clean($this->input->post("_school"));

		$data = array(
			'username' => $username,
			'password' => $password,
			'nisn' => $nisn,
			'name' => $name,
			'school_id' => $school
		);

		if ($this->student->add($data) == true) {
			header("Location:" . base_url("welcome/login?message=register-success"));
		}else{
			header("Location:" . base_url("welcome/register?message=register-failed"));
		}
	}

	public function logout()
	{
		session_destroy();
		header("Location:" . base_url('welcome/login'));
	}
}

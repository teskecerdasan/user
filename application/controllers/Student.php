<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username']) || !isset($_SESSION['type'])) {
			header("Location:" . base_url("welcome/login"));
        }
        $this->load->model("student_model", 'student');
        $this->load->model("teacher_model", 'teacher');
        $this->load->model("school_model", 'school');
        $this->load->model("question_model", 'question');
        $this->load->model("statistic_model", 'statistic');
    }

    public function lists()
    {
        $page = 1;
		$perPage = 12;
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
        }

        $student = $this->student->getAll($page, $perPage);
        $appName = $this->config->item("appName");
        $data = array('appName' => $appName, 'student' => $student, 'type' => $this->session->userdata("type"));
        $profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);
		$this->load->view('pages/student/home');
		$this->load->view('part/footer', $data);
    }

    public function add()
    {
        $school = $this->teacher->get()->school_id;
        $appName = $this->config->item("appName");
        $data = array('appName' => $appName, 'schoolId' => $school, 'type' => $this->session->userdata("type"));
        $profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);
		$this->load->view('pages/student/add');
		$this->load->view('part/footer', $data);
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        $school = $this->teacher->get()->school_id;
        $student = $this->student->getById($id);
        $appName = $this->config->item("appName");
        $data = array('appName' => $appName, 'student' => $student, 'id' => $id, 'schoolId' => $school, 'type' => $this->session->userdata("type"));
        $profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);
		$this->load->view('pages/student/edit', $data);
		$this->load->view('part/footer', $data);
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        $student = $this->student->getById($id);
        $statistic = $this->statistic->getFromTeacher($student->studentUsername);
        $category = [];
        $num = [];
        if ($statistic != null) {
            for ($i=0; $i < count($statistic); $i++) { 
                array_push($category, $statistic[$i]->categoryName);
                array_push($num, $statistic[$i]->statisticValue);
            }    
        }else{
            $categoryRaw = $this->question->getAllCategory();
            $category = [];
            for ($i=0; $i < count($categoryRaw); $i++) { 
                array_push($category, $categoryRaw[$i]->categoryName);
                array_push($num, 0);
            }
        }
        $appName = $this->config->item("appName");
        $data = array('appName' => $appName, 'student' => $student, 'label' => $category, 'num' => $num, 'type' => $this->session->userdata("type"));
        $profile;
		if ($_SESSION['type'] == "student") {
			$profile = $this->student->getProfile();
			$data['name'] = $profile->studentName;
		}else if($_SESSION['type'] == "teacher") {
			$profile = $this->teacher->get();
			$data['name'] = $profile->name;
		}
		$this->load->view('part/header', $data);
		$this->load->view('pages/student/detail', $data);
		$this->load->view('part/footer', $data);
    }

    public function doAdd()
    {
        $password = password_hash($this->input->post("password"), PASSWORD_BCRYPT);
        $data = array(
            'name' => $this->input->post("name"),
            'username' => $this->input->post("username"),
            'password' => $password,
            'nisn' => $this->input->post("nisn"),
            'school_id' => $this->input->post("school"),
            'grade_mat' => $this->input->post("grade_mat"),
            'grade_ipa' => $this->input->post("grade_ipa"),
            'grade_ips' => $this->input->post("grade_ips"),
            'grade_bhs' => $this->input->post("grade_bhs")
        );

        if ($this->student->add($data) == true) {
        	header("Location:" . base_url("student/lists?message=success-add"));
        } else {
            header("Location:" . base_url("student/lists?message=error-add"));
        }
    }

    public function doEdit()
    {
        $data = array(
            'name' => $this->input->post("name"),
            'username' => $this->input->post("username"),
            'nisn' => $this->input->post("nisn"),
            'school_id' => $this->input->post("school"),
            'updated_at' => date("Y-m-d H:i:s", time()+60*60*7),
            'grade_mat' => $this->input->post("grade_mat"),
            'grade_ipa' => $this->input->post("grade_ipa"),
            'grade_ips' => $this->input->post("grade_ips"),
            'grade_bhs' => $this->input->post("grade_bhs")
        );

        $id = $this->input->post("id");
        if($this->student->edit($id, $data) == true){
        	header("Location:" . base_url("student/lists?message=success-edit"));
        } else {
            header("Location:" . base_url("student/lists?message=error-edit"));
        }
    }


    public function doDelete()
	{
		$id = $this->uri->segment(3);
		if ($this->student->delete($id) == true) {
			header("Location:" . base_url("student/lists?message=success-delete"));
		}else{
			header("Location:" . base_url("student/lists?message=error-delete"));
		}
	}
}
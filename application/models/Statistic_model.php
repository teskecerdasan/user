<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistic_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $query = $this->db->insert_batch('statistic', $data);
        
        if ($query == true) {
            $this->db->where("username", $data[0]->student_username);
            $update = $this->db->update("student", array("tested" => 1));
            if ($update == true) {
                return true;
            } else {
                return false;
            }            
        } else {
            return false;
        }
    }

    public function getFromStudent()
    {
        $this->db->select("statistic.value AS `statisticValue`, category.name AS `categoryName`");
        $this->db->where("statistic.student_username", $_SESSION['username']);
        $this->db->from("statistic");
        $this->db->join("category", "category.id = statistic.category_id");

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        } 
    }

    public function getFromTeacher($username)
    {
        $this->db->select("statistic.value AS `statisticValue`, category.name AS `categoryName`");
        $this->db->where("statistic.student_username", $username);
        $this->db->from("statistic");
        $this->db->join("category", "category.id = statistic.category_id");

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        } 
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($page, $perPage)
    {
        $page = ($page - 1) * $perPage;

        $this->db->select("id AS `questionId`, content AS `questionContent`, category_id AS `categoryId` ");
        $this->db->limit($perPage, $page);
        $query = $this->db->get("question");

        if ($query->num_rows() > 0) {
            $data['results'] = $query->result_object();
            $data['total'] = $this->db->count_all_results("question");
            return $data;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $query = $this->db->get_where("question", array("id" => $id));
        
        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }
    }

    public function getAllCategory()
    {
        $this->db->select("id AS `category_id, name AS `categoryName");
        $query = $this->db->get("category");

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function getCategoryById($id)
    {
        $this->db->select("id AS `category_id, name AS `categoryName, description AS `categoryDescription");
        $query = $this->db->get_where("category", array("id" => $id));

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_model extends CI_Model {
    public function getPassword($username)
    {
        $this->db->select("password");
        return $this->db->get_where("teacher", array("username" => $username))->result_object()[0];
    }

    public function get()
    {
        $username = $this->session->userdata("username");
        $this->db->select("name, school_id, username, nuptk, created_at, id");
        $this->db->where("username", $username);
        $query = $this->db->get("teacher");

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }  
    }

    public function changePassword($password)
    {
        $this->db->where('username', $_SESSION['username']);
        $query = $this->db->update('teacher', array("password" => $password));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function editProfile($data)
    {
        $this->db->where('username', $_SESSION['username']);
        $query = $this->db->update('teacher', $data);

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }
}
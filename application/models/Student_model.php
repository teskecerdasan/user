<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {
    public function getPassword($username)
    {
        $this->db->select("password");
        return $this->db->get_where("student", array("username" => $username))->result_object()[0];
    }

    public function add($data)
    {
        $query = $this->db->insert('student', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($page, $perPage)
    {
        $page --;
        $this->db->select("student.id AS `studentId`, student.name AS `studentName`, student.nisn AS `studentNisn`, school.id AS `schoolId`, school.name AS `schoolName`, student.grade_mat AS `studentGradeMat`, student.grade_ipa AS `studentGradeIpa`, student.grade_ips AS `studentGradeIps`, student.grade_bhs AS `studentGradeBhs`");
        $this->db->from('student');
        $this->db->limit($perPage, $page);
        $this->db->join('school', 'school.id = student.school_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function getById($id)
    {
        $this->db->select("student.id AS `studentId`, student.name AS `studentName`, student.nisn AS `studentNisn`, student.username AS `studentUsername`, school.id AS `schoolId`, school.name AS `schoolName`, student.grade_mat AS `studentGradeMat`, student.grade_ipa AS `studentGradeIpa`, student.grade_ips AS `studentGradeIps`, student.grade_bhs AS `studentGradeBhs`, student.tested AS `studentHasTested`");
        $this->db->where('student.id', $id);
        $this->db->from('student');
        $this->db->join('school', 'school.id = student.school_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function getProfile()
    {
        $this->db->select("student.id AS `studentId`, student.name AS `studentName`, student.nisn AS `studentNisn`, student.username AS `studentUsername`, school.id AS `schoolId`, school.name AS `schoolName`, student.grade_mat AS `studentGradeMat`, student.grade_ipa AS `studentGradeIpa`, student.grade_ips AS `studentGradeIps`, student.grade_bhs AS `studentGradeBhs`, student.tested AS `studentHasTested`");
        $this->db->where('student.username', $_SESSION['username']);
        $this->db->from('student');
        $this->db->join('school', 'school.id = student.school_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function edit($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('student', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = $this->db->delete('student', array('id' => $id));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function changePassword($password)
    {
        $this->db->where('username', $_SESSION['username']);
        $query = $this->db->update('student', array("password" => $password));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }
}
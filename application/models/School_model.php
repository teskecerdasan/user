<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($page, $perPage)
    {
        $page --;
        $this->db->select("id, name, npsn");
        $query = $this->db->get("school", $page, $perPage);

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function getById($id)
    {
        $this->db->select("id, name, npsn");
        $query = $this->db->get_where("school", array("id" => $id));
        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

}
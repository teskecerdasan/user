<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                <?php foreach ($question as $item) { ?>
                <div class="col-md-12">
                    <h4><?= $num++ ?>. <?= $item->questionContent ?></h4>
                    <div class="form-group col-md-2">
                        <div class="radio">
                            <label>
                                <input type="radio" onClick="answer('<?= $item->questionId?>', '<?= $item->categoryId?>', '5')" name="<?= $item->questionId?>">
                                Sangat Setuju
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="radio">
                            <label>
                                <input type="radio" onClick="answer('<?= $item->questionId?>', '<?= $item->categoryId?>', '4')" name="<?= $item->questionId?>">
                                Setuju
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="radio">
                            <label>
                                <input type="radio" onClick="answer('<?= $item->questionId?>', '<?= $item->categoryId?>', '3')" name="<?= $item->questionId?>">
                                Kurang Setuju
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="radio">
                            <label>
                                <input type="radio" onClick="answer('<?= $item->questionId?>', '<?= $item->categoryId?>', '2')" name="<?= $item->questionId?>">
                                Tidak Setuju
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="radio">
                            <label>
                                <input type="radio" onClick="answer('<?= $item->questionId?>', '<?= $item->categoryId?>', '1')" name="<?= $item->questionId?>">
                                Sangat Tidak Setuju
                            </label>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
                <?php if ($endPage == true) { echo $links ?>
                    <a href="/quiz/total" class="btn btn-primary btn-flat">Submit</a>
                <?php }else{ 
                    echo $links;
                } ?>
                

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script>
    function answer(qId, cId, value) {
        $.ajax({
            method : "POST",
            url : "/quiz/doAnswer",
            data : {
                questionId : qId,
                categoryId : cId,
                value : value
            },
            success : function(res){
                // console.log("added");
                // console.log(res);
            }
        })
    }
                    
  </script>
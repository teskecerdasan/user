<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Student
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="/student/doAdd">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
                </div>

                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" class="form-control" id="username" placeholder="Enter username" name="username">
                </div>

                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
                </div>

                <div class="form-group">
                  <label for="nisn">NISN</label>
                  <input type="text" class="form-control" id="nisn" placeholder="Enter NISN" name="nisn">
                </div>

                <div class="form-group">
                  <label for="grade_mat">Grade Math <small>(Optional)</small></label>
                  <input type="text" class="form-control" id="grade_mat" placeholder="Enter Grade Math" name="grade_mat">
                </div>

                <div class="form-group">
                  <label for="grade_ipa">Grade IPA <small>(Optional)</small></label>
                  <input type="text" class="form-control" id="grade_ipa" placeholder="Enter Grade IPA" name="grade_ipa">
                </div>

                <div class="form-group">
                  <label for="grade_ips">Grade IPS <small>(Optional)</small></label>
                  <input type="text" class="form-control" id="grade_ips" placeholder="Enter Grade IPS" name="grade_ips">
                </div>

                <div class="form-group">
                  <label for="grade_bhs">Grade Lang <small>(Optional)</small></label>
                  <input type="text" class="form-control" id="grade_bhs" placeholder="Enter Grade Lang" name="grade_bhs">
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="text" name="school" value="<?= $schoolId ?>" hidden>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
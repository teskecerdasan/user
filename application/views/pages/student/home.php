<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Student
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Student</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>NISN</th>
                  <th>School</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $n=1; foreach ($student as $item) { ?>
                  <tr>
                    <td><?= $n++ ?></td>
                    <td><?= $item->studentName?></td>
                    <td><?= $item->studentNisn?></td>
                    <td><?= $item->schoolName?></td>
                    <td>
                        <a href="/student/detail/<?= $item->studentId ?>" class="btn btn-flat btn-primary">Detail</a>
                        <a href="/student/edit/<?= $item->studentId ?>" class="btn btn-flat btn-warning">Edit</a>
                        <a href="/student/doDelete/<?= $item->studentId ?>" class="btn btn-flat btn-danger">Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>NISN</th>
                  <th>School</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>